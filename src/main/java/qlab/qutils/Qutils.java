package qlab.qutils;

import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import qlab.qutils.blocks.BlockInit;
import qlab.qutils.items.ItemInit;
import qlab.qutils.utils.CustomCreativeTab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(Qutils.MODID)
public class Qutils {

    public static final String MODID = "qutils";

    private static final Logger LOGGER = LogManager.getLogger();

    public Qutils() {

        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();

        BlockInit.init(bus);
        ItemInit.init(bus);

    }

    public static final CustomCreativeTab QUTILS_TAB = new CustomCreativeTab(MODID, ItemInit.REINFORCEDGRAVEL_ITEM);

}
