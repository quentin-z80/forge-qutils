package qlab.qutils.utils;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.registries.RegistryObject;

public class CustomCreativeTab extends CreativeModeTab {

    private final RegistryObject<Item> item;
    
    public CustomCreativeTab(final String ID, final RegistryObject<Item> item) {
        super(ID);
        
        this.item = item;
    }

    @Override
    public ItemStack makeIcon() {
        return item.get().getDefaultInstance();
    }
}
