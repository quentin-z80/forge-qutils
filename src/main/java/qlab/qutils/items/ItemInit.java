package qlab.qutils.items;

import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import qlab.qutils.Qutils;
import qlab.qutils.blocks.BlockInit;

public class ItemInit {

    static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Qutils.MODID);

    public static void init(IEventBus bus) {

        ITEMS.register(bus);

    }

    // block items

    public static final RegistryObject<Item> REINFORCEDGRAVEL_ITEM = ITEMS.register("reinforced_gravel", () ->
        new BlockItem(BlockInit.REINFORCEDGRAVEL.get(),
        new Item.Properties().tab(Qutils.QUTILS_TAB)));
    
}
