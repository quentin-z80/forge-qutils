package qlab.qutils.blocks;

import net.minecraft.world.level.block.Block;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import qlab.qutils.Qutils;

public class BlockInit {

    static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Qutils.MODID);

    public static void init(IEventBus bus) {

        BLOCKS.register(bus);
    
    }

    public static final RegistryObject<Block> REINFORCEDGRAVEL = BLOCKS.register("reinforced_gravel", () -> new ReinforcedGravel());

}
